// @ts-nocheck
import * as mqtt from 'mqtt/dist/mqtt.min';
import { defineStore } from 'pinia'
import * as mathjs from "mathjs"
import { GenerateNodeKey, TestGroupKey, GenerateGroupKeyFromKeys, toBytes, DecryptFromString, toHex, DecryptFromStringWithFunction } from "./PTIOT"

function MacStringToInt(mac: string): number {
    return parseInt(mac.split(':').join(''), 16);
}

function IntToMacString(mac: number): string {
    return mac.toString(16)     // "4a8926c44578"
        .match(/.{1,2}/g) // ["4a", "89", "26", "c4", "45", "78"]
        .join(':')               // "78:45:c4:26:89:4a"
}
function modInverse(a, m) {
    // validate inputs
    [a, m] = [Number(a), Number(m)]
    if (Number.isNaN(a) || Number.isNaN(m)) {
        return NaN // invalid input
    }
    a = (a % m + m) % m
    if (!a || m < 2) {
        return NaN // invalid input
    }
    // find the gcd
    const s = []
    let b = m
    while (b) {
        [a, b] = [b, a % b]
        s.push({ a, b })
    }
    if (a !== 1) {
        return NaN // inverse does not exists
    }
    // find the inverse
    let x = 1
    let y = 0
    for (let i = s.length - 2; i >= 0; --i) {
        [x, y] = [y, x - y * Math.floor(s[i].a / s[i].b)]
    }
    return (y % m + m) % m
}
var mqtt_client

export const useAppStore = defineStore({
    id: "main",
    state: (): any => {
        return {
            nodes: {
            },
            edges: {
            },
            client: {},
            currentNode: "",
            currentgroup: {},
            aliasNameList: {},
            log_Data: [],
            rootData: "",
            nodeKey: "",
            StorageKeys: {},
            prioLog: 0,
            controlSetting: {},
            currentApp: "",
            groupKey: ""
        }
    },
    actions: {
        start() {
            // console.log(import.meta)
            // console.log(import.meta.env.VITE_MQTT_URL, import.meta.env.VITE_MQTT_USERNAME, import.meta.env.VITE_MQTT_PASSWORD, this.StorageKeys)
            // TestGroupKey()
            var keys = JSON.parse(localStorage.getItem("Keys"));
            this.StorageKeys = keys != null ? keys : {};
            var controlSetting = JSON.parse(localStorage.getItem("Control"));
            this.controlSetting = controlSetting != null ? controlSetting : {};
            this.log_Data = JSON.parse(localStorage.getItem("Logs"));
            console.log(import.meta.env)
            console.log(import.meta.env.VITE_MQTT_URL)
            mqtt_client = mqtt.connect(import.meta.env.VITE_MQTT_URL, { username: import.meta.env.VITE_MQTT_USERNAME, password: import.meta.env.VITE_MQTT_PASSWORD })
            mqtt_client.on('connect', function () {
                console.log("Connected")
                mqtt_client.subscribe("/#", { qos: 0 })
                mqtt_client.publish("/ping", "pingAll")
                // mqtt_client.publish("/data/root", "3/0000014000005E9B0000206A00008A16")
            })
            mqtt_client.on('message', function (topic: any, message: any) {
                const app = useAppStore();
                app.receiveMessage(topic, String.fromCharCode(...message));

            })

        },
        receiveMessage(topic: string, message: string) {
            console.log(topic, message)
            var t = topic.split("/")
            console.log(t, topic)
            try {
                var cmd = t[1]
                console.log(cmd)
                t.shift()
                t.shift()
                this[cmd](t, message.split("/"))
            } catch (error) {
                return
            }
        },
        onSubmit(e: any) {
            console.log(e)
            console.log("onSubmit")
            var mes = { topic: "", message: "" };
            try {
                mes = this[e["type"]](e)
            } catch (error) {
                mes = { topic: "", message: "" };
            }
            if (mes["topic"] != "") {
                console.log(mes)
                mqtt_client.publish(String(mes["topic"]), String(mes["message"]))
            }
        },
        ping(topic: any, message: any) {
            if (topic[0] != "response")
                return;
            this.aliasNameList[message[0]] = message[1]
            if (message[2] == "root") {
                this.nodes[message[0]] = { name: message[1] != "" ? message[1] : message[0], size: 48, color: 'white', active: false, value: isNaN(Number(message[3]))?0:Number(message[3]) }
            } else {
                var mac = IntToMacString(MacStringToInt(message[2]) - 1);
                this.nodes[message[0]] = { name: message[1] != "" ? message[1] : message[0], size: 32, color: 'white', active: false, value: isNaN(Number(message[3]))?0:Number(message[3]) }
                this.edges[`${mac}-${message[0]}`] = { source: mac, target: message[0] }
            }
        },
        data(topic: any, message: any) {
            if (message.length != 2) {
                return
            }
            if (topic[0] == "root") {
                this.rootData = message[1]
                console.log(message[1])
                for (var i in this.controlSetting[this.currentApp]) {
                    if (i == "")
                        continue;
                    try {
                        var key = this.StorageKeys[this.currentApp]
                        var value = DecryptFromStringWithFunction(key.key, message[1], this.controlSetting[this.currentApp][i], key.size, key.p2)
                        mqtt_client.publish(`/node/control/${i}`, toHex(value))
                        this.addLog(`${value} -> Node ${i} `)
                    } catch (error) {
                        console.log(error)
                    }
                }
            }
            var log = { time: new Date().toTimeString(), from: message[0], to: topic[1], message: message[1] }
            this.log_Data.push(log)
        }, status(topic: any, message: any) {
            if (topic.length != 0)
                return
            this.nodes[message[0]]["color"] = Number(message[1]) == 1 ? "blue" : Number(message[1]) == 0 ? "white" : "red";
        }, updateValue(topic: any, message: any) {
            if (topic.length != 0)
                return
            this.nodes[message[0]]["value"] = Number(message[1]);
        }, mesh(topic: any, message: any) {
            switch (message[0]) {
                case "root":
                    console.log("root", MacStringToInt(message[1]), this.aliasNameList)
                    var mac = IntToMacString(MacStringToInt(message[1]) - 1);
                    this.nodes[mac] = { name: mac in this.aliasNameList ? this.aliasNameList[mac] != "" ? this.aliasNameList[mac] : mac : mac, size: 48, color: 'white', active: false, value: 0 }
                    break;
                case "connected":
                    var mac = IntToMacString(MacStringToInt(message[2]) - 1);
                    this.nodes[message[2]] = { name: message[2] in this.aliasNameList ? this.aliasNameList[message[2]] != "" ? this.aliasNameList[mamessage[2]] : message[2] : message[2], size: 32, color: 'white', active: false, value: 0 }
                    this.edges[`${message[2]}-${message[3]}`] = { source: message[2], target: message[3] }
                    break;
                case "disconnected":
                    var mac = IntToMacString(MacStringToInt(message[2]) - 1);
                    this.nodes[message[3]]
                    break;
            }
            console.log(this.nodes)
        }, clearLog() {
            this.log_Data = []
            localStorage.setItem("Logs", JSON.stringify(this.log_Data));
        }, addLog(message: any, prio: number = 0) {
            if (prio < this.prioLog)
                return
            this.log_Data.unshift({ timestamp: new Date().toLocaleString("en-GB"), detail: message })
            localStorage.setItem("Logs", JSON.stringify(this.log_Data));
            console.log(this.log_Data)
        }, toggleLog() {
            if (this.prioLog == 0)
                this.prioLog = 1;
            else
                this.prioLog = 0

        }
    },
    getters: {
        eventHanders() {
            return {
                "node:click": ({ node }) => {
                    console.log(node)
                    this.nodeKey = ""
                    this.currentNode = node;
                }
            }
        },
        groupSubmit() {
            return (o: any) => {
                console.log(o)
                try {
                    if (o.P0 * o.P1 >= 0xFFFFFF || o.size < 2 || o.P0 < 2 || o.P1 < 2) {
                        return { topic: "", message: "" }
                    }
                } catch (error) {
                    return { topic: "", message: "" }
                }
                this.currentgroup = o;
                return { topic: `/group/setting/${this.currentNode}`, message: `${o.P0},${o.P1},${o.size}` }
            }
        },
        nodeSubmit() {
            return (o: any) => {
                console.log(o, this.currentgroup)
                var message = ""
                try {
                    if (o.p < 2) {
                        return { topic: "", message: message }
                    }
                    switch (o.node_type) {
                        case "1":
                        case "2":
                            o.d = modInverse(Number(o.e), Number(this.currentgroup["P1"]));
                            this.nodeKey = GenerateNodeKey(Number(o.e), Number(o.d), Number(o.p), Number(this.currentgroup["P0"]) * Number(this.currentgroup["P1"]), Number(this.currentgroup.size), Number(o.id))
                            if (o.node_type == "1") {
                                message = `1,${o.e},${o.id},${o.p},${o.m},${o.v},${o.A}`
                            } else {
                                message = `2,${o.e},${o.id},${o.p},${o.A}`
                            }
                            break;
                        case "3":
                            console.log("Control")
                            this.nodeKey = ""
                            message = `3,${o.id},${o.p},${o.T_l},${o.T_h}`
                            break;
                    }

                } catch (error) {
                    console.log(error)
                    return { topic: "", message: "" }
                }
                return { topic: `/node/setting/${this.currentNode}`, message: message }
            }
        },
        nameSubmit() {
            return (o: any) => {
                try {
                    if (o.name == "")
                        return { topic: "", message: "" }
                } catch (error) {
                    return { topic: "", message: "" }
                }
                return {
                    topic: `/node/name/${this.currentNode}`, message: `${o.name}`
                }
            }
        },
        valueSubmit() {
            return (o: any) => {
                try {
                    if (o.value >= 65535)
                        return { topic: "", message: "" }
                } catch (error) {
                    return { topic: "", message: "" }
                }
                return {
                    topic: `/node/data/${this.currentNode}`, message: `${o.value}`
                }
            }
        },
        controlSubmit() {
            return (o: any) => {
                try {
                    if (o.cmd > 2)
                        return { topic: "", message: "" }
                } catch {
                    return { topic: "", message: "" }
                }
                return {
                    topic: `/control/${this.currentNode}`, message: `${o.cmd}`
                }
            }
        },
        keyGroupGenerate() {
            return (o: any) => {
                var res = { topic: "", message: "" }
                var size = o.size
                var nodeKeys: [] = Array(Number(o.size)).fill(0)
                delete o["type"]
                for (var k in o) {
                    if (k == "name")
                        continue;
                    var n = Number(k)
                    if (!Number.isInteger(n))
                        continue
                    nodeKeys[n - 1] = toBytes(o[k])
                }
                console.log(nodeKeys)
                nodeKeys = nodeKeys.flat()
                console.log(nodeKeys)
                if (o.name == "" || Number(o.p2) < 100 || Number(size) < 1)
                    return res
                var key = GenerateGroupKeyFromKeys(nodeKeys, Number(size), Number(o.p2))
                this.StorageKeys[o.name] = {
                    key: key, p2: Number(o.p2), size: size
                }
                this.groupKey = toHex(key)
                localStorage.setItem("Keys", JSON.stringify(this.StorageKeys));
                this.addLog(`Generate key -> ${o.name} `, 1)
                return res
            }
        },
        valueDecode() {
            return (o: any) => {
                var res = { topic: "", message: "" }
                if (o.name == "")
                    return res;
                if (typeof this.StorageKeys[o.name] == undefined)
                    return res;
                var key = this.StorageKeys[o.name]
                var value = DecryptFromString(key.key, o.data, key.size, key.p2)
                this.addLog(`Decrypted ${o.data} -> ${toHex(value)}`, 1)
                return res;
            }
        }, valueDecodeWithFunction() {
            return (o: any) => {
                var res = { topic: "", message: "" }
                if (o.name == "")
                    return res;
                if (typeof this.StorageKeys[o.name] == undefined)
                    return res;
                var key = this.StorageKeys[o.name]
                var value = DecryptFromStringWithFunction(key.key, o.data, o.function, key.size, key.p2)
                this.addLog(`Function Decrypted ${o.data} -> ${toHex(value)}`, 1)
                return res;
            }
        }, settingSubmit() {
            return (o: any) => {
                var res = { topic: "", message: "" }
                try {
                    if (o.name == "")
                        return res;
                    if (o.name != this.currentApp) {
                        this.currentApp = o.name;
                        return res;
                    }
                    console.log(this.currentApp)
                    if (!(this.currentApp in this.controlSetting))
                        this.controlSetting[this.currentApp] = {}
                    this.controlSetting[this.currentApp][o.node] = o.function
                    localStorage.setItem("Control", JSON.stringify(this.controlSetting));
                    this.addLog(`${o.node} set  ${o.function}`, 1)
                } catch (error) {
                    console.log(error)
                    return res;
                }
                return res;
            }
        },
        CurrentControlSetting() {
            if (this.currentApp == "") {
                return []
            }
            return this.controlSetting[this.currentApp]
        },
        removeCurrentControl() {
            return (o: any) => {
                var res = { topic: "", message: "" }
                try {
                    console.log(o)
                    if (!(this.currentApp in this.controlSetting))
                        return
                    delete this.controlSetting[this.currentApp][o]
                    localStorage.setItem("Control", JSON.stringify(this.controlSetting));
                    this.addLog(`${this.currentApp} delete  ${o}`, 1)
                } catch (error) {
                    console.log(error)
                    return res;
                }
                return res;
            }
        }
    }
})

// 000031DA000012B10000734F00003E46
// 12762 4785 29519