import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import "./assets/css/icons.css";
const app = createApp(App)

app.mount('#app')
import "bootstrap"