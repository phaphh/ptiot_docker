
function sqr(x: number, m: number): number {
    return (x * x) % m
}

function pow(x: number, y: number, m: number): number {
    if (y == 0) return 1
    else if (y % 2 == 0)
        return sqr(pow(x, Math.floor(y / 2), m), m)
    else return (x * (sqr(pow(x, Math.floor(y / 2), m), m))) % m
}

export function toBytes(x: string): number[] {
    var res: number[] = []
    if (x.length % 8 != 0) return res;
    for (var p = 0; p < x.length; p += 8) {
        res.push(parseInt(x.substring(p, p + 8), 16))
    }
    return res
}

function _multipleMatrix(x: number[], y: number[], size: number, m: number): number[] {
    var res: number[] = Array(size).fill(-1)
    if (x.length != size * size || y.length != size)
        return res
    for (var i = 0; i < size; i++) {
        var c = 0;
        for (var j = 0; j < size; j++) {
            c = (c + (x[i * size + j] * y[j])) % m
            c = c < 0 ? c + m : c
        }
        res[i] = c
    }
    return res;
}

export function toHex(x: number[]): string {
    var res = "";
    for (var i = 0; i < x.length; i++)
        res += ("0000000" + (Number(x[i]).toString(16))).slice(-8).toUpperCase();
    return res;
}

export function MatrixTranspose(m: number[], size: number): number[] {
    var res: number[] = Array(size * size).fill(0)
    for (var i = 0; i < size; i++) {
        for (var j = 0; j < size; j++) {
            res[i * size + j] = m[j * size + i]
        }
    }
    return res
}

export function GenerateNodeKey(e: number, d: number, p: number, p2: number, size: number, id: number): string {
    var key: number[] = Array(size).fill(0)
    console.log(e, d, p, p2, size, id)
    key[id] = d;
    for (var temp = e, i = id + 1; i < size; i++) {
        key[i] = temp = pow(temp, p, p2)
    }

    return toHex(key)
}
export function GenarateGroupKey(nodekeys: number[], size: number, p2: number): number[] {
    var res: number[] = Array(size * size).fill(0)
    for (var i = 0; i < size; i++) {
        for (var j = 0; j < size; j++) {
            if (i == j) {
                res[i * size + j] = nodekeys[i * size + j]
                for (var k = 0; k < j; k++) {
                    res[i * size + k] = (res[i * size + k] * res[i * size + j]) % p2
                }
                break;
            }

            for (var k = 0; k < j + 1; k++) {
                res[i * size + k] = (res[i * size + k] - (nodekeys[i * size + j] * res[j * size + k])) % p2
            }
        }
    }
    for (var i = 0; i < res.length; i++)
        res[i] = res[i] < 0 ? res[i] + p2 : res[i]
    return res;
}

export function GenerateGroupKeyFromKeys(nodekeys: number[], size: number, p2: number): number[] {
    var nodekey = MatrixTranspose(nodekeys, size)
    return GenarateGroupKey(nodekey, size, p2)
}

export function DecryptFromString(groupKeys: number[], value: string, size: number, p2: number): number[] {
    return _multipleMatrix(groupKeys, toBytes(value), size, p2)
}

function EvalFunction(x: number[], func: string, p: number): number[] {
    var res: number[] = [0]
    if (func.includes("/")) {
        for (var i = 0, c = 0, pos = 0, pos_div = 0, status = 0; i < func.length; i++) {
            if (i == func.length - 1 && "+-*/".includes(func[i])) {
                return [NaN];
            }
            switch (status) {
                case 0:
                    switch (func[i]) {
                        case "/":
                            pos_div = i;
                            status = 1;
                            break;
                        case "+": case "-":
                            res[0] = (res[0] + Number(eval(func.substring(pos + 1, i)))) % p;
                            pos = i;
                            break;
                        default: break;
                    }
                    if (i == func.length - 1) {
                        res[0] = (res[0] + Number(eval(func.substring(pos + 1, i + 1)))) % p;
                    }
                    break;
                case 1:
                    switch (func[i]) {
                        case '/':
                            pos = pos_div;
                            pos_div = i;
                            status = 2;
                            break;
                        case '+': case "-":
                            res.push(Number(eval(func.substring(pos, pos_div))) % p)
                            res.push(Number(eval(func.substring(pos_div + 1, i))) % p)
                            pos = i;
                            status = 0
                            // 
                            break;
                        default: break;
                    }
                    if (i == func.length - 1) {
                        res.push(Number(eval(func.substring(pos, pos_div))) % p);
                        res.push(Number(eval(func.substring(pos_div + 1, i))) % p);
                    }
                    break;
                case 2:
                    if ("/*+-".includes(func[i]) || i == func.length - 1) {
                        func = func.substring(0, pos + 1)
                            + "(" + func.substring(pos + 1, pos_div)
                            + "*" + func.substring(pos_div + 1, i)
                            + ")" + func.substring(i)
                        return EvalFunction(x, func)
                    }
                    break;
            }
        }
    } else {
        res[0] = Number(eval(func)) % p;
    }

    return res;
}

export function DecryptFromStringWithFunction(groupKeys: number[], value: string, func: string, size: number, p2: number): number[] {
    var matrix = DecryptFromString(groupKeys, value, size, p2)
    var res = EvalFunction(matrix, func)
    for (var i = 0; i < res.length; i++) {
        res[i] = res[i] % p2;
    }
    return res
}

export function TestGroupKey() {
    var nodekey: number[] = [274, 0, 0, 0, 28627, 369, 0, 0, 13855, 5288, 45, 0, 6596, 8652, 10572, 345]
    var size: number = 4
    var m: number = 37459
    var res: number[] = GenarateGroupKey(nodekey, size, m)
    console.log(res)
    res = MatrixTranspose(res, size)
    console.log(res)
    res = MatrixTranspose(res, size)
    console.log(res)
    var value: number[] = [320, 24219, 8298, 35350]
    console.log(toHex(value))
    var v = _multipleMatrix(res, value, size, m)


    console.log(v)
    v = DecryptFromString(res, toHex(value), size, m)
    console.log(v)

    console.log(EvalFunction([1, 2, 3], "x[0]/x[1]/x[2]+x[2]*237/324"))
}