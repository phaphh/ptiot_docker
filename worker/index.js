
const redis = require("redis");
const mqtt = require("mqtt");
const axios = require("axios");
var backupServer = []
if (process.env.SERVER_BACKUP != "NULL" && String(process.env.SERVER_BACKUP).length > 5) {
    backupServer = process.env.SERVER_BACKUP.split(",");
}

const nodeID = process.env.NODE_ID;
console.log(process.env.REDIS_HOST, process.env.REDIS_PORT)
console.log(backupServer)
const redisClient = redis.createClient({ url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}` });

redisClient.on('ready', () => {
    console.log("Redis Connected!");
});
redisClient.connect()
const mqttClient = mqtt.connect(`mqtt://${process.env.MQTT_HOST}:${process.env.MQTT_PORT}`, { username: process.env.MQTT_USERNAME, password: process.env.MQTT_PASSWORD })

mqttClient.on('connect', function () {
    console.log("Connected")
    mqttClient.subscribe("/data/root", { qos: 0 })
})

function _convert02(str) {
    return ("0" + str).slice(-2)
}

function updateRedis(time, value) {
    var key = `${nodeID}/${time.getFullYear()}:${_convert02(time.getMonth() + 1)}:${_convert02(time.getDate())}`
    var hash = `${_convert02(time.getHours())}:${_convert02(time.getMinutes())}:${_convert02(time.getSeconds())}`
    console.log(hash)
    redisClient.hSet(key, hash, value)
}

function backup(time, value) {
    try {
        for (var i = 0; i < backupServer.length; i++) {
            var req = `${backupServer[i]}/update?id=${nodeID}&time=${time.getTime()}&value=${value}`
            console.log(req)
            fetch(req).then(response => response.text()).then(x => x).catch(x => x)
        }
    } catch (error) {
        // console.log(error)
    }

}

mqttClient.on('message', function (topic, message) {
    var mess = String.fromCharCode(...message).split("/")
    console.log(mess[1])
    if (mess.length != 2 && String(mess[1]).length % 8 != 0)
        return;
    var time = new Date()
    updateRedis(time, mess[1])
    backup(time, mess[1])
})