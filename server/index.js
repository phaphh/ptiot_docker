const express = require("express")
const cors = require("cors")
const redis = require("redis");
const app = express()
app.use(cors())

const redisClient = redis.createClient({ url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}` });

redisClient.on('ready', () => {
    console.log("Redis Connected!");
});
redisClient.connect()

app.get("/ping", (req, res) => {
    console.log("Ping")
    res.send("pong");
})

function _convert02(str) {
    return ("0" + str).slice(-2)
}


function updateRedis(id, time, value) {
    var _time = new Date(time)
    var key = `${id}/${_time.getFullYear()}:${_convert02(_time.getMonth() + 1)}:${_convert02(_time.getDate())}`
    var hash = `${_convert02(_time.getHours())}:${_convert02(_time.getMinutes())}:${_convert02(_time.getSeconds())}`
    redisClient.HSET(key, hash, value)
}

app.get("/update", (req, res) => {
    try {
        console.log("Update")
        updateRedis(req.query.id, Number(req.query.time), req.query.value)
        res.send("OK")
        res.status(200).end()
    } catch (error) {
        console.log(error)
        res.status(404).end()
    }
})

app.get("/data", async (req, res) => {
    try {
        var id = req.query.id
        var _time = new Date(Number(req.query.time))
        var key = `${id}/${_time.getFullYear()}:${_convert02(_time.getMonth() + 1)}:${_convert02(_time.getDate())}`
        var hash = `${_convert02(_time.getHours())}:${_convert02(_time.getMinutes())}:${_convert02(_time.getSeconds())}`
        var Data = await redisClient.HGET(key, hash)
        console.log(JSON.stringify(Data, null, 2));
        res.send(Data)
    } catch (error) {
        console.log(error)
        res.status(404).end()
    }
})

app.get("/ddata", async (req, res) => {
    try {
        var id = req.query.id
        var _time = new Date(Number(req.query.time))
        var key = `${id}/${_time.getFullYear()}:${_convert02(_time.getMonth() + 1)}:${_convert02(_time.getDate())}`
        var Data = await redisClient.HGETALL(key)
        console.log(JSON.stringify(Data, null, 2));
        res.send(Data)
    } catch (error) {
        res.status(404).end()
    }
})

app.listen(80, (err) => {
    console.log("Listening")
})